<?php 
require_once('modelo/Post.class.php');
require_once('lib/Conection.class.php');
class PostControle{
	public function addPost($post){
		$conection = new Conection("lib/mysql.ini");
		$sql = "INSERT INTO post(title, subtitle, content, postType) VALUES (:title, :subtitle, :content, :type);";
		$comando = $conection->getConection()->prepare($sql);
		$title = $post->getTitle();
		$subtitle = $post->getSubtitle();
		$content = $post->getContent();
		$type = $post->getPostType();
		$comando->bindParam("title", $title);
		$comando->bindParam("subtitle", $subtitle);
		$comando->bindParam("content", $content);
		$comando->bindParam("type", $type);

		$comando->execute();
		$id = $conection->getConection()->lastInsertId();
		$conection->__destruct();
		return $id;
	}
	public function updatePost($post){
		$conection = new Conection("lib/mysql.ini");
		$sql = "UPDATE post SET title=:title, subtitle=:subtitle, content=:content, postType=:type where id_post=:id";
		$comando = $conection->getConection()->prepare($sql);
		$title = $post->getTitle();
		$subtitle = $post->getSubtitle();
		$content = $post->getContent();
		$type = $post->getPostType();
		$id = $post->getId_Post();
		$comando->bindParam("title", $title);
		$comando->bindParam("subtitle", $subtitle);
		$comando->bindParam("content", $content);
		$comando->bindParam("type", $type);
		$comando->bindParam("id", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function deletePost($id){
		$conection = new Conection("lib/mysql.ini");
		$sql = "DELETE FROM post WHERE id_post=:id";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam("id", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function addImage($foto){
		$conection = new Conection("lib/mysql.ini");
		$sql = "INSERT INTO image(dataImage, typeImage, id_postImage) VALUES(:data, :type, :id_post);";
		$comando = $conection->getConection()->prepare($sql);
		$data = $foto->getImageData();
		$type = $foto->getImageType();
		$id = $foto->getId_Post();
		$comando->bindParam("data", $data);
		$comando->bindParam("type", $type);
		$comando->bindParam("id_post", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function updateImage($foto){
		$conection = new Conection("lib/mysql.ini");
		$sql = "UPDATE image SET dataImage=:data, typeImage=:type where id_postImage=:id;";
		$comando = $conection->getConection()->prepare($sql);
		$data = $foto->getImageData();
		$type = $foto->getImageType();
		$id = $foto->getId_Post();
		$comando->bindParam("data", $data);
		$comando->bindParam("type", $type);
		$comando->bindParam("id", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function deleteImage($id){
		$conection = new Conection("lib/mysql.ini");
		$sql = "DELETE FROM image WHERE id_postImage=:id";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam("id", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function addVideo($video){
		$conection = new Conection("lib/mysql.ini");
		$sql = "INSERT INTO video(dataVideo, typeVideo, id_postVideo) VALUES(:data, :type, :id_post);";
		$comando = $conection->getConection()->prepare($sql);
		$data = $video->getVideoData();
		$type= $video->getVideoType();
		$id = $video->getId_Post();
		$comando->bindParam("data", $data);
		$comando->bindParam("type", $type);
		$comando->bindParam("id_post", $id);
		$comando->execute();
		$conection->__destruct();
	}
		public function updateVideo($video){
		$conection = new Conection("lib/mysql.ini");
		$sql = "UPDATE video SET dataVideo=:data, typeVideo=:type where id_postVideo=:id;";
		$comando = $conection->getConection()->prepare($sql);
		$data = $video->getVideoData();
		$type= $video->getVideoType();
		$id = $video->getId_Post();
		$comando->bindParam("data", $data);
		$comando->bindParam("type", $type);
		$comando->bindParam("id", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function deleteVideo($id){
		$conection = new Conection("lib/mysql.ini");
		$sql = "DELETE FROM video WHERE id_postVideo=:id";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam("id", $id);
		$comando->execute();
		$conection->__destruct();
	}
	public function allPosts(){
		$conection = new Conection("lib/mysql.ini");
		$sql = "SELECT * FROM post as p join video as v on p.id_post = v.id_postVideo inner join image as i on p.id_post = i.id_postImage";
		$comando = $conection->getConection()->prepare($sql);
		$comando->execute();
		$resu = $comando->fetchAll();
		$conection->__destruct();
		$arrayPosts = [];
		foreach($resu as $item){
			$post = new Post();
			$post->setId_Post($item->id_post);
			$post->setTitle($item->title);
			$post->setSubtitle($item->subtitle);
			$post->setContent($item->content);
			$post->setPostType($item->postType);
			$post->setImageData($item->dataImage);
			$post->setImageType($item->typeImage);
			$post->setVideoData($item->dataVideo);
			$post->setVideoType($item->typeVideo);
			array_unshift($arrayPosts, $post);
		}
		return $arrayPosts;
	}
	public function onlyAPost($id){
		$conection = new Conection("lib/mysql.ini");
		$sql = "SELECT * FROM post as p join video as v on p.id_post = v.id_postVideo inner join image as i on p.id_post = i.id_postImage where p.id_post=:id";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam("id", $id);
		$comando->execute();
		$item = $comando->fetch();
		$conection->__destruct();
		$post = new Post();
		$post->setId_Post($item->id_post);
		$post->setTitle($item->title);
		$post->setSubtitle($item->subtitle);
		$post->setContent($item->content);
		$post->setPostType($item->postType);
		$post->setImageData($item->dataImage);
		$post->setImageType($item->typeImage);
		$post->setVideoData($item->dataVideo);
		$post->setVideoType($item->typeVideo);
		return $post;
	}
	public function allPostType($type){
		$conection = new Conection("lib/mysql.ini");
		$sql = "SELECT * FROM post as p join video as v on p.id_post = v.id_postVideo inner join image as i on p.id_post = i.id_postImage where postType=:type";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam("type", $type);
		$comando->execute();
		$resu = $comando->fetchAll();
		$conection->__destruct();
		$arrayPosts = [];
		foreach($resu as $item){
			$post = new Post();
			$post->setId_Post($item->id_post);
			$post->setTitle($item->title);
			$post->setSubtitle($item->subtitle);
			$post->setContent($item->content);
			$post->setPostType($item->postType);
			$post->setImageData($item->dataImage);
			$post->setImageType($item->typeImage);
			$post->setVideoData($item->dataVideo);
			$post->setVideoType($item->typeVideo);
			array_unshift($arrayPosts, $post);
		}
		return $arrayPosts;
	}
}