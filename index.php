<?php
echo "
<link rel='stylesheet' href='sui/semantic.min.css'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<link rel='stylesheet' href='css/own.css'>
<link rel='stylesheet href='css/princessSophia.css' />
<div class='ui labeled icon vertical inverted sidebar menu'>
	<div class='ui header item'>
		<img class='ui massive image' id='logoSide' src='LogoCVDD.png'>
	</div>
	<a href='index.php' class='active item'>
		<i class='home icon'></i>
		Início
	</a>
	<a class='header item' href='content.php?type=tuto'>
		<i class='newspaper outline icon'></i>
		Ultimas Postagens Sobre:
	</a>
	<a class='item' href='content.php?type=cursos'>
		<i class='book icon'></i>
		Cursos
	</a>
	<a class='item' href='content.php?type=projetos'>
		<i class='clipboard outline icon'></i>
		Projetos
	</a>
	<a class='item' href='content.php?type=eventos'>
		<i class='map marker icon'></i>
		Eventos
	</a>
	"; 
//Conteudo admin
if(isset($_COOKIE['admin'])){
echo "
	<div class='borderless item'></div>
	<div class='item'></div>
	<div class='header item'>Menu Admin</div>
	<a href='addPost.php' class='item'>
		<i class='plus icon'></i>
		Adicionar Posts
	</a>
	<a href='editLogin.php' class='item'>
		<i class='user outline icon'></i>
		Configurações de Conta
	</a>
	<a href='logout.php' class='item'>
		<i class='close icon icon'></i>
		Logout
	</a>
";
}
//Fim do menu
echo "
</div>
<div class='pusher'>
	<div id='fundo1' class='fundoAll'>
		<center>
			<img class='ui middle aligned image' id='logo' src='LogoCVDD.png'>
			<span id='logoText'>Casa de Vovó Dedé</span>
		</center>
	</div>
	<div class='ui inverted two item menu' id='menuFixo'>
		<center>
			<div class='item borderless'>
				<a id='sidebar' class='ui inverted teal button'>Menu</a>
			</div>
		</center>
	</div>
	<div id='fundo2-1' class='fundoAll'>
		<div id='fundo2-2'>
			<h1 class='big header ps'>A Casa de Vovó Dedé</h1>
			<p>A Casa de Vovó Dedé é uma instituição sem fins lucrativos fundada em 1993 na Barra do Ceará, em Fortaleza/CE. Nossa missão é promover o desenvolvimento humano, pessoal e profissional, por meio da arte, cultura e educação de crianças e jovens de seis a vinte e nove anos em situação de vulnerabilidade social. Levamos música, tecnologia e cultura para a nossa comunidade, através de atividades e projetos totalmente gratuitos. O amor é o principal pilar da nossa formação e rege todas as nossas ações. Acreditamos, assim, na formação e na mudança de realidade das pessoas através da arte e da educação.
			</p><br>
			<p>A Casa de Vovó Dedé é reconhecida como entidade de utilidade pública Municipal (Lei nº 8.031 de 1997), Estadual (Lei nº 12.735 de 1997) e Federal (Por decreto publicado no diário oficial de união de 1998). Temos como valores o compromisso com a comunidade, a ética e a igualdade de gênero, racial e social. Divulgamos anualmente a prestação de contas de todas as nossas atividades. 
			</p>
		</div>
	</div>
	<div class='fundo3'><h1 class='ps'>O Que Disponibilizamos</h1><br>
		<p>
		Além de promover a disseminação da arte, cultura e tecnologia por meio de <span class='textLink'><a href='content.php?type=projetos'>projetos</a></span>, oficinas e <span class='textLink'><a href='content.php?type=cursos'>cursos</a></span>, a Casa também colabora para o crescimento de seus alunos e professores com <span class='textLink'><a href='content.php?type=eventos'>eventos</a></span> de alta qualidade elaborados por toda a equipe da Casa. Acreditamos que o conhecimento atrelado a experiências reais em qualquer área de expertise são essenciais para a formação de um bom profissional.
		</p><br>
		<p>A Casa de Vovó Dedé como agente de formação artística e cultural promove formações de grupos musicais e apresentações buscando sempre a participação dos alunos, professores e convidados, o que contribui para o aperfeiçoamento musical de todos. Sabemos que a formação de grupos musicais também funciona como uma ferramenta social de interação, pois esse trabalho coletivo estimula a convivência e ajuda ao próximo.
		</p><br>
		<p>A Casa através de diversos cursos com o objetivo de proporcionar aos jovens importantes ferramentas de formação profissional, tais como: fotografia, audiovisual, animação, design gráfico, entre outros, se propõe a ser um importante canal de fomento e divulgação da cultura da nossa comunidade através da produtora de audiovisual, da TVDD, da Rádio RVDD, do Estúdio Casa Animada, da Fábrica de Software, entre outros. 
		</p><br>



	</div>
	<br>
	<div class='ui divider footerDivider'></div>
	<div class='ui inverted segment' id='footer'>
		<div class='ui inverted six columns stackable grid'>
			<div class='column'></div>
			<div class='column'></div>
			<div class='column'>
				Rua Jerônimo de Albuquerque, 445 <br>
				Barra do Ceará, Fortaleza-CE <br>
				Próximo a AMBEV <br>
				<i class='whatsapp icon'></i>86 8769-7341
			</div>
			<div class='column'>
				Criado por: Bruno Paiva
				Disponibilização dos Conteúdos: <br>
				Casa de Vovó Dedé<br>
				Fábrica de Software/Facebook
			</div>
			<div class='column'></div>
			<div class='column'></div>
			</center>
		</div>
	</div>
</div>
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
<script>
$('#menuFixo').visibility({
	type: 'fixed'
});
$('#sidebar').click(function(){
	$('.ui.sidebar').sidebar('toggle');
});
</script>
";
?>