<?php
if(!isset($_COOKIE['admin'])){
	header("Location: index.php");
}
require_once("controle/PostControle.class.php");
$post = new PostControle();
$id = $_GET['id'];
$post->deleteImage($id);
$post->deleteVideo($id);
$post->deletePost($id);
header("location: content.php");
