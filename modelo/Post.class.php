<?php
class Post{
	private $id_postImage;
	private $id_post;
	private $id_postVideo;
	private $title;
	private $subtitle;
	private $content;
	private $postType;
	private $imageData;
	private $imageType;
	private $videoData;
	private $videoType;
	public function getId_Post(){
		return $this->id_post;
	}
	public function setId_Post($i){
		$this->id_post = $i;
	}
	public function getTitle(){
		return $this->title;
	}
	public function getSubtitle(){
		return $this->subtitle;
	}
	public function getContent(){
		return $this->content;
	}
	public function getPostType(){
		return $this->postType;
	}
	public function getImageData(){
		return $this->imageData;
	}
	public function getImageType(){
		return $this->imageType;
	}
	public function getVideoData(){
		return $this->videoData;
	}
	public function getVideoType(){
		return $this->videoType;
	}

	public function setTitle($t){
		$this->title = $t;
	}
	public function setSubtitle($s){
		$this->subtitle = $s;
	}
	public function setContent($c){
		$this->content = $c;
	}
	public function setPostType($pT){
		$this->postType = $pT;
	}
	public function setImageData($iD){
		$this->imageData = $iD;
	}
	public function setImageType($iT){
		$this->imageType = $iT;
	}
	public function setVideoData($vD){
		$this->videoData = $vD;
	}
	public function setVideoType($vT){
		$this->videoType = $vT;
	}
}
?>