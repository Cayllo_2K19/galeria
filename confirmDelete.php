<?php

echo "
<link rel='stylesheet' href='sui/semantic.min.css'>
<link rel='stylesheet' href='css/own.css'>
<div class='ui basic tiny modal'>
	<div class='header'>Você realmente deseja apagar essa publicação?</div>
	<div class='description'>Essa publicação não poderá ser recuperada depois que você apagar, então tome muito cuidado.</div>
	<div class='actions'>
		<a href='content.php' class='ui red cancel button'>Não apagar</a>
		<a href='delete.php?id={$_GET['id']}' class='ui green ok button'>Apagar</a>
	</div>
</div>
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
<script>
$().ready(function(){
	$('.ui.basic.modal')
	  .modal('show')
	;
});
</script>";
?>