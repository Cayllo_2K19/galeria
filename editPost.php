<?php
if(!isset($_COOKIE['admin'])){
	header("Location: index.php");
}
require_once("controle/PostControle.class.php");
$post = new PostControle();
$postzin = $post->onlyAPost($_GET['id']);
echo "
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'/>
<link rel='stylesheet' href='sui/semantic.min.css'>
<link rel='stylesheet' href='css/own.css'>
<link rel='stylesheet' href='css/oldOwn.css'>
<link rel='stylesheet' href='froala/font-awesome/css/font-awesome.min.css'>
<link rel='stylesheet' href='froala/css/froala_editor.css'>
<link rel='stylesheet' href='froala/css/froala_style.css'>
<link rel='stylesheet' href='froala/css/plugins/code_view.css'>
<link rel='stylesheet' href='froala/css/plugins/draggable.css'>
<link rel='stylesheet' href='froala/css/plugins/colors.css'>
<link rel='stylesheet' href='froala/css/plugins/emoticons.css'>
<link rel='stylesheet' href='froala/css/plugins/image_manager.css'>
<link rel='stylesheet' href='froala/css/plugins/image.css'>
<link rel='stylesheet' href='froala/css/plugins/line_breaker.css'>
<link rel='stylesheet' href='froala/css/plugins/table.css'>
<link rel='stylesheet' href='froala/css/plugins/char_counter.css'>
<link rel='stylesheet' href='froala/css/plugins/video.css'>
<link rel='stylesheet' href='froala/css/plugins/fullscreen.css'>
<link rel='stylesheet' href='froala/css/plugins/file.css'>
<link rel='stylesheet' href='froala/css/plugins/quick_insert.css'>
<link rel='stylesheet' href='froala/css/plugins/help.css'>
<link rel='stylesheet' href='froala/css/third_party/spell_checker.css'>
<link rel='stylesheet' href='froala/css/plugins/special_characters.css'>
<link rel='stylesheet' href='froala/css/codemirror.min.css'>
<style> 
body, .pusher, .pusher.dimmed{
overflow-y: auto;
}
</style>
<div class='ui labeled icon vertical inverted thin sidebar menu'>
	<div class='ui header item'>
		<img class='ui massive image' id='logoSide' src='LogoCVDD.png'>
	</div>
	<a href='index.php' class='item'>
		<i class='home icon'></i>
		Início
	</a>
	<a class='header item' href='content.php?type=tuto'>
		<i class='newspaper outline icon'></i>
		Ultimas Postagens Sobre:
	</a>
	<a class='item' href='content.php?type=cursos'>
		<i class='book icon'></i>
		Cursos
	</a>
	<a class='item' href='content.php?type=projetos'>
		<i class='clipboard outline icon'></i>
		Projetos
	</a>
	<a class='item' href='content.php?type=eventos'>
		<i class='map marker icon'></i>
		Eventos
	</a>
	"; 
//Conteudo admin
if(isset($_COOKIE['admin'])){
echo "
	<div class='borderless item'></div>
	<div class='item'></div>
	<div class='header item'>Menu Admin</div>
	<a href='content.php' class='active item'>
		<i class='plus icon'></i>
		Adicionar Posts
	</a>
	<a href='editLogin.php' class='item'>
		<i class='user outline icon'></i>
		Configurações de Conta
	</a>
	<a href='logout.php' class='item'>
		<i class='close icon icon'></i>
		Logout
	</a>
";
}
//Fim do menu
echo "
</div>
<div class='pusher'>
	<div class='ui inverted two item fixed menu'>
		<center>
			<div class='item borderless'>
				<a id='sidebar' class='ui inverted teal button'>Menu</a>
			</div>
		</center>
	</div>
	<div class='ui inverted two item menu'>
		<center>
			<div class='item borderless'>
			</div>
		</center>
	</div>
	<div class='ui inverted divider'></div>	
	<h1 class='ps'>Editar o conteúdo da publicação</h1>
	<div class='ui inverted divider'></div>	
	<br>
	<form class='ui inverted form' action='editPostBack.php?id=". "{$_GET['id']}"  ."'method='POST' enctype='multipart/form-data'>
		<div class='fundoPost'>
			<center>
				<div class='fields'>
					<div class='three wide field'></div>
					<div class='ten wide field'>
						<label for='title'>Título da Publicação: </label>
						<input type='text' id='title' value='{$postzin->getTitle()}' name='title' maxlength='50' required />
					</div>
					<div class='three wide field'></div>
				</div>
				<br>
				<div class='fields'>
					<div class='three wide field'></div>
					<div class='ten wide field'>
						<label for='subtitle'>Subtítulo da Publicação: </label>
						<input type='text' id='subtitle' value='{$postzin->getSubtitle()}' name='subtitle' maxlength='115' required />
					</div>
					<div class='three wide field'></div>
				</div>
				<br>
				<div class='fields'>
					<div class='five wide field'></div>
					<div class='six wide field'>
						<label>Categoria da Publicação: </label>
						<select name='type' required />
							<option value='{$postzin->getPostType()}'>Selecione uma categoria</option>
							<option value='cursos'>Curso</option>
							<option value='projetos'>Projetos</option>
							<option value='eventos'>Eventos</option>
						</select>
					</div>
					<div class='five wide field'></div>
				</div>
				<br>
				<div class='ui inverted divider'></div>
				<h2 style='color:cyan;'>Inserção de arquivos</h2>
				<br>
				<div class='fields'>
					<div class='five wide field'></div>
					<div class='six wide field'>
						<label type='file' class='ui inverted teal button inputFile' for='img'>Adicionar/Editar Imagem<input style='display:none;' type='file' name='img' id='img' accept='image/*' /></label>
					</div>
					<div class='five wide field'></div>
				</div>
				<div class='fields'>
					<div class='five wide field'></div>
					<div class='six wide field'>
						<label type='file' class='ui inverted teal button inputFile' for='video'>Adicionar/Editar Vídeo <input style='display:none;' type='file' name='video' id='video' accept='video/*' /></label>
					</div>
					<div class='five wide field'></div>
				</div>
				<div class='ui inverted divider'></div>
			</center>
		</div>
		<br>
		<div id='froalaDiv'>
			<fieldset>
				<legend><h1>Conteúdo da Publicação</h1></legend>
				<textarea name='content' id='froala' placeholder='mopa' required>{$postzin->getContent()}</textarea>
			</fieldset>
		</div>
		<br><br><br>
		<input class='ui inverted teal button' type='submit' value='Editar Publicação' />
		<br><br><br><br><br><br>
	</form>
</div>
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
" . #Importações froala#
"
<script type='text/javascript' src='froala/js/codemirror.min.js'></script>
<script type='text/javascript' src='froala/js/xml.min.js'></script>
<script type='text/javascript' src='froala/js/froala_editor.min.js' ></script>
<script type='text/javascript' src='froala/js/plugins/align.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/char_counter.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/code_beautifier.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/code_view.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/colors.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/draggable.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/emoticons.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/entities.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/file.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/font_size.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/font_family.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/fullscreen.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/line_breaker.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/inline_style.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/link.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/lists.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/paragraph_format.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/paragraph_style.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/quick_insert.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/quote.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/table.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/save.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/url.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/help.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/print.min.js'></script>
<script type='text/javascript' src='froala/js/third_party/spell_checker.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/special_characters.min.js'></script>
<script type='text/javascript' src='froala/js/plugins/word_paste.min.js'></script>
<script>
$('#sidebar').click(function(){
	$('.ui.sidebar').sidebar('toggle');
});
$('#froala').froalaEditor({
	toolbarButtons: ['html','undo', 'redo' , '|', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'color', '|', 'fontFamily', 'fontSize', '|', 'align', 'formatOL', 'formatUL', 'insertLink', 'clearFormatting', 'insertTable', 'save'],
		toolbarButtonsXS: ['undo', 'redo' , '-', 'bold', 'italic', 'underline'],
		language: 'pt_br',
		height:300,
		placeholderText: 'Digite aqui o conteúdo da sua postagem...'
});
</script>
	";
?>